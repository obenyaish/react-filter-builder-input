import React, { useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { usePopper } from "react-popper";

export type Props = {
  value: string;
  setValue: (string) => any;
  options?: string[];
  isLoadingOptions?: boolean;
  onEnter?: (newValue) => any;
  onBackspace?: () => any;
  onArrowDown?: () => any;
  onEnteredDirectlyToInput?: (string) => any;
  loadingIndicator?: React.ReactNode | null | undefined;
  inputStyle?: React.CSSProperties;
};

const BACKSPACE_KEY_CODE = 8;
const ENTER_KEY_CODE = 13;
const KEY_DOWN_KEY_CODE = 40;
const KEY_UP_KEY_CODE = 38;

function FilterInput({
  value,
  setValue,
  options = [],
  isLoadingOptions,
  onEnter,
  onBackspace,
  onArrowDown,
  onBlur,
  onEnteredDirectlyToInput,
  loadingIndicator,
  inputStyle,
  ...props
}: Props & React.InputHTMLAttributes<HTMLInputElement>) {
  const [inputRef, setInputRef] = useState(null);
  const [referenceElement, setReferenceElement] = useState(null);
  const [isListOpen, setIsListOpen] = useState(true);
  const [popperElement, setPopperElement] = useState(null);
  const { styles, attributes, forceUpdate } = usePopper(
    referenceElement,
    popperElement,
    {
      placement: "bottom-start",
    }
  );
  const [itemIndex, setItemIndex] = useState(-1);
  const [selectedValue, setSelectedValue] = useState<string | undefined>();

  function selectOneBelow() {
    if (itemIndex === options.length - 1) {
      setSelectedValue(undefined);
    } else {
      setSelectedValue(options[itemIndex + 1]);
    }
  }

  function selectOneAbove() {
    if (itemIndex === -1) {
      setSelectedValue(options[options.length - 1]);
    } else if (itemIndex === 0) {
      setSelectedValue(undefined);
    } else {
      setSelectedValue(options[itemIndex - 1]);
    }
  }

  function handleKeyPress(event: React.KeyboardEvent) {
    if (event.keyCode === ENTER_KEY_CODE && onEnter) {
      if (selectedValue) {
        onEnter(selectedValue);
      } else {
        onEnter(value);
      }
    }

    if (event.keyCode === BACKSPACE_KEY_CODE && onBackspace) {
      onBackspace();
    }

    if (event.keyCode === KEY_DOWN_KEY_CODE) {
      event.preventDefault();
      onArrowDown && onArrowDown();
      selectOneBelow();
    }

    if (event.keyCode === KEY_UP_KEY_CODE) {
      event.preventDefault();
      selectOneAbove();
    }
  }

  useEffect(() => {
    if (selectedValue) {
      setItemIndex(options.findIndex((v) => v === selectedValue));
    } else {
      setItemIndex(-1);
    }
  }, [selectedValue, options]);

  useEffect(() => {
    if (forceUpdate) {
      forceUpdate();
    }
  }, [forceUpdate]);

  useEffect(() => {
    if (inputRef) {
      inputRef.focus();
    }
  }, [inputRef]);

  return (
    <>
      <div ref={setReferenceElement}>
        <NoBorderInput
          {...props}
          onBlur={(e) => {
            onBlur && onBlur(e);
            setIsListOpen(false);
          }}
          onFocus={() => {
            setIsListOpen(true);
          }}
          ref={setInputRef}
          value={value}
          onChange={(e) => {
            setValue(e.target.value);
            onEnteredDirectlyToInput &&
              onEnteredDirectlyToInput(e.target.value);
          }}
          onKeyDown={handleKeyPress}
          style={inputStyle}
        />
      </div>
      <div ref={setPopperElement} style={styles.popper} {...attributes.popper}>
        {isListOpen && options.length > 0 && !isLoadingOptions && (
          <ListContainer>
            {options.map((item, index) => (
              <ListItem
                key={"itemkeyinlist" + index}
                isSelected={index === itemIndex}
                onMouseDown={(e) => {
                  e.preventDefault();
                  onEnter(item);
                }}
                style={inputStyle}
              >
                {item}
              </ListItem>
            ))}
          </ListContainer>
        )}
        {isLoadingOptions && (
          <ListContainer>
            {loadingIndicator ? (
              loadingIndicator
            ) : (
              <LoadingContainer>loading...</LoadingContainer>
            )}
          </ListContainer>
        )}
      </div>
    </>
  );
}

export default FilterInput;

const NoBorderInput = styled.input`
  border-width: 0;
  height: 2.5rem;
  font-size: 1.1rem;
  max-width: 10rem;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
    "Noto Sans", Ubuntu, Cantarell, "Helvetica Neue", sans-serif,
    "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";

  &:focus {
    outline: none;
  }
`;

const ListContainer = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 10rem;
  padding-top: 0.7rem;
  padding-bottom: 0.7rem;
  background-color: #fafafa;
  border-width: 1px;
  border-style: solid;
  border-color: rgb(219, 219, 219, 1);
  border-radius: 0.5rem;
  box-shadow: 0 2px 4px rgb(0 0 0 / 10%);
  max-height: 30rem;
  overflow-y: auto;
`;

const ListItemDiv = ({
  isSelected,
  children,
  ...props
}: { isSelected?: boolean } & React.HTMLAttributes<HTMLDivElement>) => (
  <div {...props}>{children}</div>
);

const ListItem = styled(ListItemDiv)`
  display: flex;
  max-width: 10rem;
  min-height: 1.1rem;
  flex-wrap: nowrap;
  overflow: hidden;
  background-color: ${(props) => (props.isSelected ? "#e0e0e0" : "#fafafa")};
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
    "Noto Sans", Ubuntu, Cantarell, "Helvetica Neue", sans-serif,
    "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
  font-size: 1rem;
  padding: 0.5rem;
  cursor: pointer;

  &:hover {
    background-color: #e0e0e0;
  }
`;

const LoadingContainer = styled.div`
  display: flex;
  max-width: 10rem;
  min-height: 1.1rem;
  flex-wrap: nowrap;
  overflow: hidden;
  background-color: #fafafa;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
    "Noto Sans", Ubuntu, Cantarell, "Helvetica Neue", sans-serif,
    "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
  font-size: 1rem;
  padding: 0.5rem;
  cursor: wait;
`;
