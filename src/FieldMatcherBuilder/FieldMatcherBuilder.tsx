import React, { useEffect, useState } from "react";
import {
  BooleanRelationOptions,
  BooleanRelationType,
  FieldMatcher,
} from "../DataTypes";
import styled from "styled-components";
import LabelChip from "../LabelChip/LabelChip";
import FilterInput from "../FilterInput";
import { LabelComponentType, LabelType, OptionsProps } from "../PropTypes";

export type Props = {
  onChange: (FieldMatcher) => any;
  value: FieldMatcher;
  onSelfDelete?: () => any;
  initialInputValue?: string;
  chipsColor?: string;
  LabelComponent?: LabelComponentType;
} & OptionsProps;

function filterArray(array: string[], value: string) {
  const lowerCaseArray = array.map((v) => v.toLowerCase());
  const lowerCaseValue = value.toLowerCase();

  if (lowerCaseArray.includes(lowerCaseValue)) {
    return array;
  }

  return array.filter((v) => v.toLowerCase().includes(lowerCaseValue));
}

function isInArray(array: string[], value: string) {
  return array.map((v) => v.toLowerCase()).includes(value.toLowerCase());
}

const FIELD_POSITION = "field";
const RELATION_POSITION = "relation";
const VALUE_POSITION = "value";
const NOT_EDITING_POSITION = "not_editing_anything";

export default function FieldMatcherBuilder({
  onChange,
  value,
  onSelfDelete,
  initialInputValue,
  chipsColor,
  relationOptions = BooleanRelationOptions,
  fieldOptions = [],
  isLoadingFieldOptions = false,
  valueOptions = [],
  isLoadingValueOptions = false,
  onOptionsContextChange,
  loadingIndicator,
  LabelComponent = LabelChip,
  inputStyle,
}: Props) {
  const [inputValue, setInputValue] = useState(
    initialInputValue ? initialInputValue : ""
  );
  const [editingPosition, setEditingPosition] = useState(FIELD_POSITION);

  useEffect(() => {
    onOptionsContextChange &&
      onOptionsContextChange(
        inputValue || "",
        value.field,
        value.relation,
        value.value
      );
  }, [inputValue, value]);

  function setField(field) {
    if (field) {
      onChange({
        ...value,
        field,
      });

      setEditingPosition(
        value.relation ? NOT_EDITING_POSITION : RELATION_POSITION
      );
    }

    setInputValue("");
  }

  function setRelation(relation) {
    if (relation) {
      onChange({
        ...value,
        relation,
      });

      setEditingPosition(value.value ? NOT_EDITING_POSITION : VALUE_POSITION);
    }

    setInputValue("");
  }

  function setValue(newFieldValue) {
    if (newFieldValue) {
      onChange({
        ...value,
        value: newFieldValue,
      });

      setEditingPosition(NOT_EDITING_POSITION);
    }

    setInputValue("");
  }

  function removeField() {
    if (inputValue === "" && !value.value) {
      onChange({
        ...value,
        field: undefined,
      });

      editField();
    }
  }

  function removeRelation() {
    if (inputValue === "" && !value.value) {
      onChange({
        ...value,
        relation: undefined,
      });

      editRelation();
    }
  }

  function resolveRelationWhenTyping(newValue) {
    if (newValue && isInArray(relationOptions, newValue)) {
      setRelation(newValue);
    }
  }

  function editField() {
    setEditingPosition(FIELD_POSITION);
    setInputValue(value.field || "");
  }

  function editRelation() {
    setEditingPosition(RELATION_POSITION);
    setInputValue(value.relation || "");
  }

  function editValue() {
    setEditingPosition(VALUE_POSITION);
    setInputValue(value.value || "");
  }

  function exitEdit() {
    if (!value.field) {
      setEditingPosition(FIELD_POSITION);
    } else if (!value.relation) {
      setEditingPosition(RELATION_POSITION);
    } else if (!value.value) {
      setEditingPosition(VALUE_POSITION);
    } else {
      setEditingPosition(NOT_EDITING_POSITION);
    }

    setInputValue("");
  }

  return (
    <BuilderContainer>
      {editingPosition == FIELD_POSITION && (
        <FilterInput
          options={fieldOptions}
          isLoadingOptions={isLoadingFieldOptions}
          loadingIndicator={loadingIndicator}
          value={inputValue}
          onBackspace={() =>
            inputValue === "" && onSelfDelete && onSelfDelete()
          }
          setValue={setInputValue}
          onEnter={setField}
          onBlur={exitEdit}
          inputStyle={inputStyle}
        />
      )}
      {value.field && editingPosition != FIELD_POSITION && (
        <LabelComponent
          label={value.field}
          onClick={editField}
          color={chipsColor}
          labelType={LabelType.FieldName}
        />
      )}
      {editingPosition == RELATION_POSITION && (
        <FilterInput
          options={filterArray(relationOptions, inputValue)}
          value={inputValue}
          setValue={setInputValue}
          onEnter={setRelation}
          onBackspace={removeField}
          onBlur={exitEdit}
          onEnteredDirectlyToInput={resolveRelationWhenTyping}
          inputStyle={inputStyle}
        />
      )}
      {value.relation && editingPosition != RELATION_POSITION && (
        <LabelComponent
          label={value.relation}
          onClick={editRelation}
          color={chipsColor}
          labelType={LabelType.BooleanRelation}
        />
      )}
      {editingPosition == VALUE_POSITION && (
        <FilterInput
          options={valueOptions}
          isLoadingOptions={isLoadingValueOptions}
          loadingIndicator={loadingIndicator}
          value={inputValue}
          setValue={setInputValue}
          onEnter={setValue}
          onBackspace={removeRelation}
          onBlur={exitEdit}
          inputStyle={inputStyle}
        />
      )}
      {value.value && editingPosition != VALUE_POSITION && (
        <LabelComponent
          label={value.value}
          onClick={editValue}
          color={chipsColor}
          labelType={LabelType.MatcherValue}
        />
      )}
    </BuilderContainer>
  );
}

const BuilderContainer = styled.div`
  display: flex;
  flex-wrap: nowrap;
  width: fit-content;
  flex-direction: row;
`;
