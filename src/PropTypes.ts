import React from "react";

export type OptionsProps = {
  relationOptions?: string[];
  fieldOptions?: string[];
  isLoadingFieldOptions?: boolean;
  valueOptions?: string[];
  isLoadingValueOptions?: boolean;
  onOptionsContextChange?: OnOptionsContextChange;
  loadingIndicator?: React.ReactNode | null | undefined;
  inputStyle?: React.CSSProperties;
};

export type OnOptionsContextChange = (
  inputValue: string,
  field: string | undefined,
  relation: string | undefined,
  value: string | undefined
) => any;

export enum LabelType {
  FieldName = "FieldName",
  BooleanRelation = "BooleanRelation",
  MatcherValue = "MatcherValue",
  LogicalRelation = "LogicalRelation",
  Brackets = "Brackets",
}

export type LabelProps = {
  label: string;
  labelType: LabelType;
  color?: string;
  onClick?: () => any;
};

export type LabelComponentType =
  | React.FunctionComponent<LabelProps>
  | ((LabelProps) => JSX.Element);
