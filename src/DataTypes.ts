export enum BooleanRelationType {
  Equals = "=",
  NotEquals = "!=",
  GreaterThan = ">",
  GreaterThanEqual = ">=",
  SmallerThan = "<",
  SmallerThanEqual = "<=",
  Contains = "Contains",
  NotContains = "Not Contains",
  Like = "Like",
}

export const BooleanRelationOptions = Object.values(
  BooleanRelationType
).map((v) => v.toString());

export enum LogicalRelationType {
  And = "And",
  Or = "Or",
}

export enum FilterElementType {
  LogicalRelation = "LogicalRelation",
  FieldMatcher = "FieldMatcher",
}

export interface FilterElement {
  type: FilterElementType;
}

export type LogicalRelation = {
  type: FilterElementType.LogicalRelation;
  relation: LogicalRelationType;
  elements: FilterElement[];
};

export type FieldMatcher = {
  type: FilterElementType.FieldMatcher;
  field: string;
  relation: string;
  value: string;
};

export function newEmptyFieldMatcher(): FieldMatcher {
  return {
    type: FilterElementType.FieldMatcher,
    value: undefined,
    field: undefined,
    relation: undefined,
  };
}

export function newEmptyLogicalRelation(): LogicalRelation {
  return {
    type: FilterElementType.LogicalRelation,
    relation: LogicalRelationType.And,
    elements: [],
  };
}
