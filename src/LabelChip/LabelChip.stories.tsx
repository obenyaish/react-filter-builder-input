import React from "react";
import LabelChip from "./LabelChip";

export default {
  title: "LabelChip",
};

export const BananaLabel = () => <LabelChip label={"banana"} />;

export const CustomColorBananaLabel = () => <LabelChip label={"banana"} color={"yellow"}/>;

export const AlertOnClickLabel = () => <LabelChip label={"banana"} onClick={() => alert("bannana!!!!")}/>;
