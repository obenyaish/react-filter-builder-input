import { LogicalRelation } from "../DataTypes";
import { LabelComponentType, OptionsProps } from "../PropTypes";

export type FilterBuilderInputProps = {
  filter: LogicalRelation | undefined;
  onChange: (LogicalRelation) => any;
  colorPalette?: string[];
  LabelComponent?: LabelComponentType;
} & OptionsProps;
